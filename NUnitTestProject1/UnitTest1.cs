using MockingUebung;
using NUnit.Framework;

namespace NUnitTestProject1
{
    public class Tests
    {

        [Test]
        public void Test1()
        {
            // Arrange
            string eingabe = "das\nist\nein\ntest";
            string[] erwartet = new string[] { "das", "ist", "ein", "test" };

            Class1 testobjekt = new Class1(new MockStreamReaderWrapper(eingabe));
            // Act

            string[] ergebnis = testobjekt.AuslesenUndUmwandeln();
            // Assert

            CollectionAssert.AreEqual(erwartet,ergebnis);
        }
    }
    class MockStreamReaderWrapper : IStreamReaderWrapper
    {
        string returnValues;

        public MockStreamReaderWrapper(string returnValues)
        {
            this.returnValues = returnValues;
        }

        public string ReadToEnd()
        {
            return returnValues;
        }
    }
}