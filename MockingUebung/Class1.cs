﻿using System;
using System.IO;
using System.Linq;

namespace MockingUebung
{
    public class Class1
    {
        IStreamReaderWrapper streamReaderWrapper;

        public Class1(IStreamReaderWrapper streamReaderWrapper)
        {
            this.streamReaderWrapper = streamReaderWrapper;
        }

        public string[] AuslesenUndUmwandeln()
        {
            string zeilen = streamReaderWrapper.ReadToEnd();
            return zeilen.Split("\n");

        }
 
    }

    public interface IStreamReaderWrapper
    {
        string ReadToEnd();
    }

    public class WrapperStreamReader : IStreamReaderWrapper
    {
        StreamReader sr;

        WrapperStreamReader(StreamReader sr)
        {
            this.sr = sr;
        }

        public string ReadToEnd()
        {
            return sr.ReadToEnd();
        }

    }
}
